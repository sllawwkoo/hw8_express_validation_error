import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export function News() {
	const [posts, setPosts] = useState([]);
	// page - поточна сторінка
	const [page, setPage] = useState(1);
	// count - кількість всіх постів
	const [count, setCount] = useState(0);
	// limit - кількість постів на сторінці
	const limit = 3;

	useEffect(() => {
		const loadPosts = async () => {
			const skip = page > 1 ? limit * (page - 1) : 0;

			const response = await fetch(
				`http://localhost:8000/api/news?skip=${skip}&limit=${limit}`
			);
			const { data } = await response.json();

			setPosts(data.items);
			setCount(data.count);
		};

		loadPosts();
	}, [page]);

	const nextPage = () => {
		if (page >= Math.ceil(count / limit)) {
			return;
		}
		setPage(page + 1);
	};

	const prevPage = () => {
		if (page === 1) {
			return;
		}
		setPage(page - 1);
	};

	return (
		<div className="container">
			<div className="titleBox">
				<h1>All News</h1>
				<Link to="newsCreateUpdate">
					<button>Create New Article</button>
				</Link>
			</div>
			<div className="newsGrid">
				{posts &&
					posts.map((item, index) => (
						<Link
							to={`/news/${item.id}`}
							key={index}
							className="newsCard"
						>
							<h2 className="newsTitle">{item.title}</h2>
							<p className="newsText">{item.text}</p>
						</Link>
					))}
			</div>
			<div className="btn_paginaton">
				<button
					id="prev"
					onClick={prevPage}
				>
					PrevPage
				</button>
				<span className="page">{`page № ${page} `}</span>
				<button
					id="next"
					onClick={nextPage}
				>
					NextPage
				</button>
			</div>
		</div>
	);
}
