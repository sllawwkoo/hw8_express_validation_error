// export default {
// 	name: "news",
// 	schema: {
// 		id: { type: "number", positive: true, integer: true },
// 		title: { type: "string", max: 50 },
// 		text: { type: "string", max: 256 },
// 		genre: { type: "enum", values: ["Politic", "Business", "Sport", "Other"] },
// 		isPrivate: { type: "boolean" },
// 		createDate: { type: "date" },
// 	},
// };

export default {
	name: "news",
	fields: [
		{
			name: "id",
			type: "increments",
		},
		{
			name: "title",
			type: "string",
		},
		{
			name: "text",
			type: "string",
		},
		{
			name: "genre",
			type: "string",
		},
		{
			name: "isPrivate",
			type: "boolean",
		},
		{
			name: "createDate",
			type: "datetime",
		},
	],
};
