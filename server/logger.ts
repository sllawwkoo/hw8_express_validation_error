import winston from "winston";

// Налаштування логування в консоль та в файл
export const logger = winston.createLogger({
	level: "info",
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.json()
	),
	transports: [
		new winston.transports.Console(),
		new winston.transports.File({ filename: "logs.log" }),
	],
});

// Логування вхідних запитів
// export function logRequest(req: Request, res: Response, next: Function): void {
//     logger.info(`Method: ${req.method}, URL: ${req.url}, Body: ${JSON.stringify(req.body)}`);
//     next();
// }

// Обробник помилок для логування
// export function logError(err: Error, req: Request, res: Response, next: Function):void {
//     logger.error(`Error: ${err.message}`);
//     next(err);
// }