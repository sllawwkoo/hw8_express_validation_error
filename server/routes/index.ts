import { Router } from "express";
import NewsRouter from "./news.routes";

interface RoutesInterface {
	[index: string]: Router;
}

const Routes: RoutesInterface = {
	news: NewsRouter,
};

export default Routes;
