import Validator, { ValidationError } from "fastest-validator";
import { logger } from "../logger";

const v = new Validator();

const schema = {
	title: { type: "string", max: 50, optional: true },
	text: { type: "string", max: 256, optional: true },
	genre: {
		type: "enum",
		values: ["Politic", "Business", "Sport", "Other"],
		optional: true,
	},
	isPrivate: { type: "boolean", optional: true },
};

class NewsValidator {
	static async validate(data: any): Promise<string[] | true> {
		const validationResult = await v.validate(data, schema);

		if (Array.isArray(validationResult)) {
			logger.error(`Validation error: ${JSON.stringify(validationResult)}`);
			return validationResult.map(
				(error: ValidationError) => error.message ?? "Unknown validation error"
			);
		}

		return true;
	}
}

export default NewsValidator;
