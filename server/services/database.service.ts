import knex from "knex";
import path from "path";
import DatabaseSchemas from "../schemas";
import NewsValidator from "./validator.service";

// клас DatabaseService це DAL (Data Access Layer) для роботи з базою даних
class DatabaseService {
	private static instance: DatabaseService;
	private db: any;

	constructor() {
		// ініціалізація бази даних
		this.db = knex({
			client: "sqlite3",
			connection: {
				// використовуємо SQLite3 для бази даних
				filename: path.join(__dirname, "../db.sqlite"),
			},
			useNullAsDefault: true,
		});
	}

	// ініціалізація екземпляра класу(патерн Singleton)
	public static getInstance(): DatabaseService {
		if (!DatabaseService.instance) {
			DatabaseService.instance = new DatabaseService();
		}
		return DatabaseService.instance;
	}

	// створення таблиць у базі даних на основі схеми
	async createTables() {
		for (const schema of DatabaseSchemas) {
			// перевірка чи таблиця існує
			const tableExists = await this.db.schema.hasTable(schema.name);

			// якщо таблиця не існує
			if (!tableExists) {
				// створення таблиці
				await this.db.schema.createTable(schema.name, (table: any) => {
					for (const field of schema.fields) {
						// створення полів з вказаними типами
						// table[field.type as string](field.name);
						if (field.name === "createDate" && field.type === "datetime") {
							table.datetime("createDate").defaultTo(this.db.fn.now());
						} else {
							table[field.type as string](field.name);
						}
					}
				});
			}
		}
	}

	// видалення таблиць
	async dropTables() {
		for (const schema of DatabaseSchemas) {
			const tableExists = await this.db.schema.hasTable(schema.name);

			if (tableExists) {
				await this.db.schema.dropTable(schema.name);
			}
		}
	}

	// метод для створення нового запису у базі даних для вказаної таблиці
	async create(table: string, data: any) {
		try {
			const validationResult = await NewsValidator.validate(data);
			if (validationResult !== true) {
				throw new Error(
					`Validation error: ${JSON.stringify(validationResult)}`
				);
			}

			return this.db
				.insert(data)
				.into(table)
				.returning("*")
				.then((rows: any) => rows[0]);
		} catch (error: any) {
			throw new Error(`Validation error: ${error.message}`);
		}
	}

	//метод для отримання усього списку записів з таблиці бази даних
	async readAll(table: string) {
		return this.db.select().from(table).returning("*");
	}
	// метод для отримання списку записів з таблиці бази даних з пагінацією
	async readList(
		table: string,
		params: {
			skip: number;
			limit: number;
		}
	) {
		const items = await this.db
			.select()
			.from(table)
			.limit(params.limit)
			.offset(params.skip);

		const count = await this.db
			.select()
			.from(table)
			.count()
			.then((rows: any) => rows[0]["count(*)"]);

		return {
			items,
			count,
		};
	}

	// метод для отримання одного запису з бази даних
	async read(table: string, id: number) {
		return this.db
			.select()
			.from(table)
			.where("id", id)
			.returning("*")
			.then((rows: any) => rows[0]);
	}

	// метод для оновлення одного запису
	async update(table: string, id: number, newData: any) {
		try {
			const validationResult = await NewsValidator.validate(newData);
			if (validationResult !== true) {
				throw new Error(
					`Validation error: ${JSON.stringify(validationResult)}`
				);
			}

			const updatedCount = await this.db(table).where("id", id).update(newData);

			if (updatedCount === 0) {
				throw new Error(`Record with id ${id} not found`);
			}

			return updatedCount;
		} catch (error: any) {
			console.error(`Error updating record: ${error.message}`);
			throw new Error(`Error updating record`);
		}
	}

	// метод для видалення одного запису
	async delete(table: string, id: number) {
		return this.db(table).where("id", id).del();
	}
}

// експортування екземпляра класу
export default DatabaseService.getInstance();
