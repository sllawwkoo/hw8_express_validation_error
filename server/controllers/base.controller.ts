import { Request, Response } from "express";
import { DatabaseService } from "../services";

// Клас BaseController є базовим класом для всіх контролерів.
export class BaseController {
	private table: string;

	// Конструктор класу BaseController, приймає назву таблиці як аргумент та зберігає її в приватній властивості table.
	constructor(table: string) {
		this.table = table;

		// Прив'язка контексту для кожного методу класу, щоб забезпечити правильний контекст `this` при виклику цих методів.
		// Це необхідно, оскільки методи будуть передаватися як обробники подій у маршрутах Express.
		this.create = this.create.bind(this);
		this.getAll = this.getAll.bind(this);
		this.getList = this.getList.bind(this);
		this.getSingle = this.getSingle.bind(this);
		this.update = this.update.bind(this);
		this.delete = this.delete.bind(this);
	}

	// Метод для створення нового запису у базі даних для вказаної таблиці.
	async create(req: Request, res: Response) {
		try {
			// Створення нового запису у базі даних
			// req.body - дані запису для створення
			const data = await DatabaseService.create(this.table, req.body);

			// Успішна відповідь з кодом статусу 201 (створено) та об'єктом з повідомленням та створеним записом.
			res.status(201);
			res.json({ message: `${this.table} created`, data });
		} catch (error) {
			// Обробка помилки при створенні запису у базі даних.
			res.status(500);
			res.json({ message: `Error creating ${this.table}` });
		}
	}

	//Метод для отримання всіх записів з бази даних для вказаної таблиці
	async getAll(req: Request, res: Response) {
		try {
			const data = await DatabaseService.readAll(this.table);

			res.status(200);
			res.json({ data });
		} catch (error) {
			res.status(404);
			res.json({ message: `${this.table} not exists` });
		}
	}

	// Метод для отримання списку записів з бази даних для вказаної таблиці з пагінацією.
	async getList(req: Request, res: Response) {
		try {
			// Визначення параметрів запиту за замовчуванням
			// limit - кількість записів, які повинні бути отримані
			// skip - кількість записів, які необхідно пропустити

			const limit = Number(req.query.limit) || 2;
			const skip = Number(req.query.skip) || 0;

			// Отримання списку записів з бази даних
			const data = await DatabaseService.readList(this.table, {
				limit,
				skip,
			});

			// Відповідь з кодом статусу 200 та об'єктом з повідомленням та списком записів
			res.status(200);
			res.json({ data });
		} catch (error) {
			// Обробка помилки при отриманні списку записів
			res.status(404);
			res.json({ message: `${this.table} not exists` });
		}
	}

	// Метод для отримання одного запису з бази даних
	async getSingle(req: Request, res: Response) {
		try {
			// Отримання одного запису з бази даних
			// req.params.id - ID запису
			const data = await DatabaseService.read(
				this.table,
				Number(req.params.id)
			);
			res.status(200);
			res.json({ data, message: `${this.table} read` });
		} catch (error) {
			res.status(404);
			res.json({ message: `${this.table} not founded` });
		}
	}

	// Метод для оновлення одного запису
	async update(req: Request, res: Response) {
		// try {
		// 	// Оновлення одного запису
		// 	// req.params.id - ID запису
		// 	// req.body - нові дані
		// 	await DatabaseService.update(this.table, Number(req.params.id), req.body);
		// 	res.status(200);
		// 	res.json({ updatedData: req.body, message: `${this.table} updated` });
		// } catch (error) {
		// 	res.status(500);
		// 	throw new Error(`Error updating ${this.table}`);
		// }
		try {
			const updatedRecord = await DatabaseService.update(
				this.table,
				Number(req.params.id),
				req.body
			);
			res.status(200).json({
				updatedData: updatedRecord,
				message: `${this.table} updated`,
			});
		} catch (error: any) {
			console.error("Error updating record:", error);
			res
				.status(500)
				.json({ error: "Error updating record", message: error.message });
		}
	}

	// Метод для видалення одного запису
	async delete(req: Request, res: Response) {
		try {
			// Видалення одного запису
			// req.params.id - ID запису
			await DatabaseService.delete(this.table, Number(req.params.id));
			res.status(200);
			res.json({ message: `${this.table} was  removed` });
		} catch (error) {
			res.status(500);
			throw new Error(`Error removing ${this.table}`);
		}
	}
}
